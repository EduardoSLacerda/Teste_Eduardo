package vek;

import javax.swing.JOptionPane;

public class Principal {

	public static void main(String[] args) {

		int taxaMaximaCredito = Integer.parseInt(JOptionPane.showInputDialog("Informe a taxa Máxima para crédito"));
		int taxaMinimaCredito = Integer.parseInt(JOptionPane.showInputDialog("Informe a taxa Mínima para crédito"));
		int taxaMaximaParcelado = Integer.parseInt(JOptionPane.showInputDialog("Informe a taxa Máxima para parcelado"));
		int taxaMinimaParcelado  = Integer.parseInt(JOptionPane.showInputDialog("Informe a taxa Mínima para parcelado"));

		int menuValores = 10;

		while (menuValores != 0){

			menuValores = Integer.parseInt(JOptionPane.showInputDialog(null, 
							"1) 1318 \n" +
							"2) 4409 \n" +
							"3) 4628 \n" +
							"4) 4430 \n","VALORES", JOptionPane.QUESTION_MESSAGE));

			switch(menuValores) {

			case 1:

				JOptionPane.showMessageDialog(null, "Nada consta nessa opção!");

				break;

			case 2:
			case 3:

				int menuOpcoes = 10;

				while (menuOpcoes != 0){

					menuOpcoes = Integer.parseInt(JOptionPane.showInputDialog(null, 
									"1) Crédito \n" +
									"2) Parcelado \n" +
									"3) Crédito e Parcelado \n" +
									"4) Débito \n","OPÇÕES", JOptionPane.QUESTION_MESSAGE));

					switch(menuOpcoes) {

					case 1:

						if(menuValores == 2) {
							JOptionPane.showMessageDialog(null, "Operação realizada!");
							menuOpcoes = 0;
						}else{

							String taxaCredito = "";

							while(taxaCredito.isEmpty() == true) {
								taxaCredito = JOptionPane.showInputDialog("Taxa de Crédito (Obrigatório)");

								if(taxaCredito.isEmpty() == true) {
									JOptionPane.showMessageDialog(null, "Preencha o campo requisitado!");
								}else if(Integer.valueOf(taxaCredito) > taxaMaximaCredito){
									JOptionPane.showMessageDialog(null, "Taxa muito alta");
									taxaCredito = "";
								}else if(Integer.valueOf(taxaCredito) < taxaMinimaCredito) {
									JOptionPane.showMessageDialog(null, "Taxa muito baixa");
									taxaCredito = "";
								}else{
									JOptionPane.showMessageDialog(null, "Operação realizada!");
								}
							}
							menuOpcoes = 0;
						}

						break;

					case 2:
					case 3:

						if(menuValores == 2) {
							JOptionPane.showMessageDialog(null, "Operação realizada!");
							menuOpcoes = 0;
						}else{

							int menuVezes = 10;
							while (menuVezes != 0){

								menuVezes = Integer.parseInt(JOptionPane.showInputDialog(null, 
												"Quantas vezes? \n" +		
												"1) 6x \n" +
												"2) 12x \n","OPÇOES", JOptionPane.QUESTION_MESSAGE));
								switch(menuVezes) {


								case 1: 
									String taxaParcelado6 = "";

									while(taxaParcelado6.isEmpty() == true) {

										taxaParcelado6 = JOptionPane.showInputDialog("Taxa parcelamento 6x:");

										if(taxaParcelado6.isEmpty() == true) {
											taxaParcelado6 = " "; // Pois não é um campo obrigatório
											menuVezes = 0;
											menuOpcoes = 0;
										}else if(Integer.valueOf(taxaParcelado6) > taxaMaximaParcelado) {
											JOptionPane.showMessageDialog(null, "Taxa muito alta");
											taxaParcelado6 = "";
										}else if(Integer.valueOf(taxaParcelado6) < taxaMinimaParcelado) {
											JOptionPane.showMessageDialog(null, "Taxa muito baixa");
											taxaParcelado6 = "";
										}else{
											JOptionPane.showMessageDialog(null, "Operação realizada!");
											menuVezes = 0;
											menuOpcoes = 0;
										}	
									}
									break;
								case 2: 
									String taxaParcelado12 = "";

									while(taxaParcelado12.isEmpty() == true) {

										taxaParcelado12 = JOptionPane.showInputDialog("Taxa parcelamento 12x:");

										if(taxaParcelado12.isEmpty() == true) {
											taxaParcelado12 = " "; // Pois não é um campo obrigatório
											menuVezes = 0;
											menuOpcoes = 0;
										}else if(Integer.valueOf(taxaParcelado12) > taxaMaximaParcelado) {
											JOptionPane.showMessageDialog(null, "Taxa muito alta");
											taxaParcelado12 = "";
										}else if(Integer.valueOf(taxaParcelado12) < taxaMinimaParcelado) {
											JOptionPane.showMessageDialog(null, "Taxa muito baixa");
											taxaParcelado12 = "";
										}else if(taxaParcelado12.equalsIgnoreCase("N/A")) {
											JOptionPane.showMessageDialog(null, "Não possue produto!"); //Somente na 12x tem a opção caso não tenha produto
											menuVezes = 0;
											menuOpcoes = 0;
										}else{
											JOptionPane.showMessageDialog(null, "Operação realizada!");
											menuVezes = 0;
											menuOpcoes = 0;
										}	
									}
									break;

								}

							}
						}
						break;

					case 4:
						JOptionPane.showMessageDialog(null, "Operação realizada!");
						menuOpcoes = 0;
						break;
					}
				}

				break;
			case 4:
				String taxaAmex = "";

				while(taxaAmex.isEmpty() == true) {
					taxaAmex = JOptionPane.showInputDialog("Taxa de Crédito (Obrigatório)");

					if(taxaAmex.isEmpty() == true) {
						JOptionPane.showMessageDialog(null, "Preencha o campo requisitado!");
					}else if(Integer.valueOf(taxaAmex) > taxaMaximaCredito){
						JOptionPane.showMessageDialog(null, "Taxa muito alta");
						taxaAmex = "";
					}else if(Integer.valueOf(taxaAmex) < taxaMinimaCredito) {
						JOptionPane.showMessageDialog(null, "Taxa muito baixa");
						taxaAmex = "";
					}else{
						JOptionPane.showMessageDialog(null, "Operação realizada!");
					}
				}
				break;
			}
		}
	}
}